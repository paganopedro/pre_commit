## O PROBLEMA

Cada um pensa de uma forma diferente e organiza o código de uma forma diferente, desta forma, como varias pessoas podem trabalhar em
um mesmo código sem que pareça que foi feito uma colagem de coisas que não seguem a mesma linha de racioncio em relação a organização.


## Pre-Commit

Com o pre-commit o código é formatado e analisado em busca de erros antes de realizar o commit.
Ao dar o comando git commit o código será formatado usando o black e na sequencia o flake8 será
executado em busca de problemas.
---

## Step by Step

Ao clonar essa pasta você ja vai ter os arquivos necessario para o pre-commit, sendo necessario apenas os passos 1, 8 e 9. Abaixo uma descrição passo a passo de todo o processo.

1. Crie o seu ambiente virtual
```
virtualenv venv -p python3

source venv/bin/activate
```

2. Crie os arquivo de configuração do pre-commit com o nome ".pre-commit-config.yaml" não esqueça que o nome do arquivo começa com ponto.

3. Coloque o código abaixo dentro do arquivo.
```
repos:
-   repo: https://github.com/ambv/black
    rev: 22.3.0
    hooks:
    - id: black
-   repo: https://github.com/PyCQA/flake8.git
    rev: 4.0.1
    hooks:
    - id: flake8

```
4. Crie o arquivo de configuração do black com o nome "pyproject.toml"

5. Coloque o código abaixo dentro do arquivo.
```
[tool.black]
      line-length = 120
      include = '\.pyi?$'
      exclude = '''
      /(
          \.git
        | \.hg
        | \.mypy_cache
        | \.tox
        | \.venv
        | _build
        | buck-out
        | build
        | dist
      )/
      '''
```
6. Crie o arquivo de configuração do flake8 com o nome ".flake8", não esqueça que o nome do arquivo começa com ponto.

7. Coloque o código abaixo dentro do arquivo.
```
[flake8]
      ignore = E203, E266, E501, W503, F403, F401
      max-line-length = 120
      max-complexity = 18
      select = B,C,E,F,W,T4,B9

```
8.  Agora vamos instalar o pre-commit

```
pip install pre-commit

```
9. Por fim vamos dar o comando
```
pre-commit install

```
Pronto, agora o processo do pre-commit ta conectado ao commit, quando voce usar o comando git commit ele vai testar os arquivos a serem commitados.

---